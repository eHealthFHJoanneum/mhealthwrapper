/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
//import com.google.gson.GsonBuilder;


public class JoltSample {

    public static void main(String[] args) throws IOException {

        // How to access the test artifacts, i.e. JSON files
        //  JsonUtils.classpathToList : assumes you put the test artifacts in your class path
        //  JsonUtils.filepathToList : you can use an absolute path to specify the files


        // https://stackoverflow.com/questions/4008223/print-in-new-line-java
        String newLine = System.getProperty("line.separator");//This will retrieve line separator dependent on OS.

        File fitbitFiles            = new File("./target/classes/json/sample/input/01-fitbit-files");
        File[] listOfFitBitFiles = fitbitFiles.listFiles();

        FileWriter myWriter;
        Object fitbitDataFile;

        List fitbitToOmhChainrSpec;
        Chainr fitbitToOmhChainrObject;
        Object omhDataFile;

        List omhToFireChainrSpec;
        Chainr omhToFireChainrObject;
        Object fireDataFile;

        for(int counter = 0; counter < listOfFitBitFiles.length; counter++) {
            System.out.println( "#######################################################################################" );
            System.out.println( "Processing file <" + counter + "> <" + listOfFitBitFiles[counter].getPath() + ">" );

            fitbitDataFile = JsonUtils.classpathToObject("/json/sample/input/01-fitbit-files/" + listOfFitBitFiles[counter].getName() );

            fitbitToOmhChainrSpec = JsonUtils.classpathToList( "/json/sample/input/02-fitbit-to-omh-spec-files/" + listOfFitBitFiles[counter].getName() );
            fitbitToOmhChainrObject = Chainr.fromSpec( fitbitToOmhChainrSpec );
            omhDataFile = fitbitToOmhChainrObject.transform( fitbitDataFile );

            myWriter = new FileWriter("./target/classes/json/sample/output/03-omh-files/" + listOfFitBitFiles[counter].getName());
            myWriter.write(JsonUtils.toPrettyJsonString( omhDataFile ));
            myWriter.close();
            System.out.println( "   output created at <" + "./target/classes/json/sample/output/03-omh-files/" + listOfFitBitFiles[counter].getName() + ">" + newLine +
                    JsonUtils.toPrettyJsonString( omhDataFile ));


            omhToFireChainrSpec = JsonUtils.classpathToList( "/json/sample/input/04-omh-to-fire-spec-files/" + listOfFitBitFiles[counter].getName());
            omhToFireChainrObject = Chainr.fromSpec( omhToFireChainrSpec );
            fireDataFile = omhToFireChainrObject.transform( omhDataFile );

            myWriter = new FileWriter("./target/classes/json/sample/output/05-fire-files/" + listOfFitBitFiles[counter].getName());
            myWriter.write(JsonUtils.toPrettyJsonString( fireDataFile ));
            myWriter.close();

            System.out.println( "   output created at <" + "./target/classes/json/sample/output/05-fire-files/" + listOfFitBitFiles[counter].getName() + ">:" + newLine +
                    JsonUtils.toPrettyJsonString( fireDataFile ));
        }

        //Read all Fitbit data files
        /*Object fitbitDataFile = JsonUtils.classpathToObject("/json/sample/01-fitbit-files/heart-rate-01.json");


        List fitbitChainrSpec = JsonUtils.classpathToList( "/json/sample/02-fitbit-to-omh-spec-files/heart-rate-01-spec.json" );
        Chainr chainr = Chainr.fromSpec( fitbitChainrSpec );

        Object omhDataFile = chainr.transform( fitbitDataFile );
        System.out.println( "Omh data file: " + JsonUtils.toJsonString( omhDataFile ) );

        myWriter = new FileWriter("./target/classes/json/sample/03-omh-files/heart-rate-01.json");
        myWriter.write(JsonUtils.toJsonString( omhDataFile ));
        myWriter.close();

        List chainrSpecFire = JsonUtils.classpathToList( "/json/sample/04-omh-to-fire-spec-files/heart-rate-01-spec.json" );
        Chainr chainrSpecFireObject = Chainr.fromSpec( chainrSpecFire );

        Object fireJson = chainrSpecFireObject.transform( omhDataFile );
        System.out.println( "Fire data file: " + JsonUtils.toJsonString( fireJson ) );

        myWriter = new FileWriter("./target/classes/json/sample/05-fire-files/heart-rate-01.json");
        myWriter.write(JsonUtils.toJsonString( fireJson ));
        myWriter.close();*/


    }
}
